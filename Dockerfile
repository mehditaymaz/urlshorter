FROM openjdk:17-jdk

EXPOSE 8080

WORKDIR /usr/local/bin/myApp

ADD   /src/main/resources  ./

ADD   /src/main/resources/db/changelog     /db/changelog

ADD    /build/libs/urlshortener-0.0.1-SNAPSHOT.jar ./server.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "./server.jar" ]