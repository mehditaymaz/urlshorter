package com.mycom.urlshortener.datasource

import com.mycom.urlshortener.datamodel.UrlShorterEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface UrlShorterRepository : JpaRepository<UrlShorterEntity, Long> {
    fun findByShortedUrl(@Param("shorted_url") sign: String): UrlShorterEntity?
}