package com.mycom.urlshortener.datamodel

import jakarta.persistence.*

@Entity
@Table(name = "url_shorter")
class UrlShorterEntity {
    constructor() : this(
            null,
            null,
            null)

    constructor(
            id: Long?,
            url: String?,
            shortedUrl: String?

    )

    @get:Id
    @get:Column(name = "id", nullable = false, insertable = false, updatable = false)
    var id: Long? = null

    @get:Basic
    @get:Column(name = "url", nullable = true)
    var url: String? = null

    @get:Basic
    @get:Column(name = "shorted_url", nullable = true)
    var shortedUrl: String? = null
}


