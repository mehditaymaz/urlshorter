package com.mycom.urlshortener.datamodel

data class UrlShorterDto(
        val url: String?,
        val shortedUrl: String?
)