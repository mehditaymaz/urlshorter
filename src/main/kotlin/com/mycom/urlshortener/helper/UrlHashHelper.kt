package com.mycom.urlshortener.helper

import java.math.BigInteger
import java.security.MessageDigest

class UrlHashHelper {

    companion object {
        fun getMd5Hash(input: String): String {
            val md = MessageDigest.getInstance("MD5")

            val messageDigest = md.digest(input.toByteArray())
            val number = BigInteger(1, messageDigest)
            var hashtext = number.toString(16)
            while (hashtext.length < 32) {
                hashtext = "0$hashtext"
            }
            return hashtext
        }
    }
}