package com.mycom.urlshortener.controller

import com.mycom.urlshortener.datamodel.UrlShorterDto
import com.mycom.urlshortener.helper.UrlHashHelper
import com.mycom.urlshortener.service.UrlShorterService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RequestMapping("/api/v1/url")
@RestController
class UrlShorterController {
    @Autowired
    lateinit var urlShorterService: UrlShorterService

    @PostMapping("/shortedurl")
    fun shortenUrl(@RequestBody url: String): ResponseEntity<String> {
        val hash = UrlHashHelper.getMd5Hash(url)
        var urlShorterDto = UrlShorterDto(url, hash)
        urlShorterService.add(urlShorterDto)
        return ResponseEntity.accepted().body(hash)
    }

    @GetMapping("/resolve/{hash}")
    fun resolveUrl(@PathVariable hash: String): String? {
        return urlShorterService.findByHash(hash)
    }

}