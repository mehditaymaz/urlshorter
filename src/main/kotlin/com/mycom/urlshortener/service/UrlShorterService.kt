package com.mycom.urlshortener.service

import com.mycom.urlshortener.datamodel.UrlShorterDto
import com.mycom.urlshortener.datamodel.UrlShorterEntity
import com.mycom.urlshortener.datasource.UrlShorterRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UrlShorterService {
    @Autowired
    lateinit var urlShorterRepository: UrlShorterRepository

    fun add(urlShorterDto: UrlShorterDto): UrlShorterDto {
        val urlShorterEntity = urlShorterRepository.save(toUrlShorterEntity(urlShorterDto))
        val urlShorterDto = UrlShorterDto(urlShorterEntity.url, urlShorterEntity.shortedUrl)
        return urlShorterDto
    }

    fun findByHash(hashUrl: String): String? {
        val urlShorterEntity = urlShorterRepository.findByShortedUrl(hashUrl)
        if (urlShorterEntity != null) {
            return urlShorterEntity.url
        }

        return null
    }

    private fun toUrlShorterEntity(urlShorterDto: UrlShorterDto): UrlShorterEntity {
        var urlShorterEntity = UrlShorterEntity()
        urlShorterEntity.url = urlShorterDto.url
        urlShorterEntity.shortedUrl = urlShorterDto.shortedUrl
        return urlShorterEntity
    }
}