package com.mycom.urlshortener.controller

import com.mycom.urlshortener.datamodel.UrlShorterDto
import com.mycom.urlshortener.service.UrlShorterService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.*

@ExtendWith(SpringExtension::class)
@WebMvcTest(UrlShorterController::class)
@ContextConfiguration(classes = [UrlShorterController::class, UrlShorterService::class])
@AutoConfigureMockMvc
class UrlShorterControllerTest {
    @MockBean
    private val urlShorterService: UrlShorterService? = null

    @Autowired
    private val mvc: MockMvc? = null

    @Test
    @Throws(Exception::class)
    fun shortenUrl_shouldReturnshortenedUrl() {
        var urlShorterDto = UrlShorterDto("www.google.com", "go.com")
        Mockito.`when`(urlShorterService?.add(urlShorterDto)).thenReturn(urlShorterDto)

        mvc!!.perform(MockMvcRequestBuilders.post("/api/v1/url/shortedurl").content("www.google.com"))
                .andExpect(MockMvcResultMatchers.status().isAccepted)
                .andReturn()
    }
}